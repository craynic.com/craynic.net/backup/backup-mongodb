FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

COPY files/ /

ENV BACKUP_CRON_EXPRESSION="" \
    BACKUP_CRON_LOGLEVEL="6" \
    BACKUP_SUCCESS_WEBHOOK="" \
    BACKUP_RESTIC_FORGET_KEEP_ARGS="" \
    BACKUP_MONGODB_ARCHIVE_FILENAME="mongodb.archive" \
    BACKUP_MONGODB_CONNECTION_STRING="" \
    BACKUP_MONGODB_AUTH_USER="" \
    BACKUP_MONGODB_AUTH_PASS="" \
    BACKUP_UNLOCK_ON_START="yes" \
    RESTIC_REPOSITORY="" \
    RESTIC_PASSWORD="" \
    ENVOY_SIDECAR_SUPPORT="no" \
    ENVOY_SIDECAR_WAIT_TIMEOUT="60"

# renovate: datasource=repology depName=alpine_3_21/restic depType=dependencies versioning=loose
ARG RESTIC_VERSION="0.17.3-r2"

RUN apk add --no-cache \
        bash~=5 \
        run-parts~=4 \
        restic="${RESTIC_VERSION}" \
        moreutils~=0 \
        curl~=8 \
        mongodb-tools~=100

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint"]
CMD ["/usr/local/sbin/docker-cmd"]
